#!/usr/bin/env sh

# Mostly taken from https://github.com/NethumL/nethuml.github.io/blob/main/build.sh
echo "Preparing"
# mkdir -p content-org/images/generated

echo "Exporting to MD"
(
    cd src || exit
    emacs --batch --no-init-file --load ../publish.el --funcall qscire-publish-all
)
