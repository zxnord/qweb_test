;;; publish.el -*- lexical-binding: t; -*-

;; Mostly taken from https://github.com/NethumL/nethuml.github.io/blob/main/publish.el

(setq qscire-files '("qscire.org" ))

;; Install packages
(require 'package)
(package-initialize)
(unless package-archive-contents
  (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-refresh-contents))
(dolist (pkg '(org-contrib ox-hugo ))
  (unless (package-installed-p pkg)
    (package-install pkg)))

(require 'url-methods)
(url-scheme-register-proxy "http")
(url-scheme-register-proxy "https")

;; Load packages
(require 'org)
(require 'ox-hugo)
(require 'ox-extra)
(ox-extras-activate '(ignore-headlines))

;; Prepare org-babel
(setq org-confirm-babel-evaluate nil)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)))

(defun qscire-publish-all ()
  (message "Publishing from emacs...")
  (dolist (elt qscire-files)
    (find-file elt)
    ; (org-babel-execute-buffer t) ;; Gives me too many problems
    (org-hugo-export-wim-to-md t)
    (message (format "Exported from %s" elt)))
  (message "Finished exporting to markdown"))

;;; publish.el ends here
